# AWS Deploy Amplify

AWS Deploy Amplify will deploy a static web site to AWS Amplify.

## Usage

```yml
include:
  - component: gitlab.com/tickett/components/aws-deploy-amplify/aws-deploy-amplify@main
    inputs:
      app_id: abcd1234
      branch_name: main
      build_job_name: build-amplify-site
      force_deploy: true
      zip_filename: release.zip

build-amplify-site:
  image: mcr.microsoft.com/dotnet/sdk:6.0
  script:
    - dotnet publish -c Release --self-contained false -r linux-arm64 -o publish
    - cd publish
    - zip -r ../release.zip *
  artifacts:
    paths:
      - release.zip
```

## Inputs

### `app_id`

The AWS Amplify application identifier.

### `branch_name`

The AWS Amplify branch name.

### `build_job_name`

The name of the job to collect the zip file artifact from.

### `force_deploy`

* Leave blank to trigger a deployment when creating a tag.
* Set to `true` to force a deployment when not creating a tag.

### `use_oidc`

The identity provider:

* Leave blank to use AWS access key / secreate.
* Set to `true` to use GitLab IdP for AWS.

### `zip_filename`

The filename for the archive containing the code to deploy to AWS Lambda.

## Variables

The AWS CLI needs to authenticate with AWS.
Configure these as project CI/CD variables and ensure they are masked/protected.

* `AWS_DEFAULT_REGION`.

### When Using OIDC

* `ROLE_ARN`:
  The AWS ARN for the role to assume.
* `AWS_PROFILE`: `oidc`.
* `AWS_CONFIG_FILE`:

  ```
  [profile oidc]
  role_arn=${ROLE_ARN}
  web_identity_token_file=${OIDC_TOKEN}
  ```

The role trust relationship should be configured similar to:

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {
                "Federated": "arn:aws:iam::<ACCOUNT_ID>:oidc-provider/gitlab.com"
            },
            "Action": "sts:AssumeRoleWithWebIdentity",
            "Condition": {
                "StringEquals": {
                    "gitlab.com:aud": "https://gitlab.com"
                },
                "StringLike": {
                    "gitlab.com:sub": "project_path:group-path/project-path:*"
                }
            }
        }
    ]
}
```

### When not Using OIDC

* `AWS_ACCESS_KEY_ID`.
* `AWS_SECRET_ACCESS_KEY`.

Use an IAM user with a deciated policy to execute a single action: `amplify:StartDeployment`
on a single resource (the AWS Aplify application):

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "amplify:StartDeployment",
            "Resource": "arn:aws:amplify:<REGION>:<ACCOUNT_ID>:apps/<APP_ID>/*"
        }
    ]
}
```
